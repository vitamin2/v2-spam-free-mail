<?php

Kirby::plugin("vitamin2/v2-spam-free-mail", [
    "snippets" => [
        "global/protected-mail" => __DIR__ . "/snippets/protected-mail.php",
    ],
    "hooks" => [
        "kirbytags:after" => function ($text) {
            if (!class_exists("Kirby\Panel\Panel", false)) {
                $dom = new DOMDocument();
                $dom->loadHTML(htmlentities("<body>" . $text . "</body>"));
                $links = $dom->getElementsByTagName("a");
                if (count($links) == 0) {
                    return $text;
                }
                foreach ($links as $link) {
                    maniplulateLinks($link, $dom);
                }
                $result = $dom->saveHTML($dom->getElementsByTagName("body")[0]);
                $output = substr($result, strlen("<body>"), -strlen("</body>"));
                return htmlspecialchars_decode($output);
            }
        },
    ],
]);

function maniplulateLinks($link, $dom)
{
    if (str_contains($link->getAttribute("href"), "mailto:")) {
        $emailAddressOnly = substr($link->getAttribute("href"), 7);
        $customAttributes = [];
        foreach ($link->attributes as $attr) {
            if ($attr->name != "href") {
                array_push($customAttributes, [
                    $attr->name => $attr->textContent,
                ]);
            }
        }
        $newCustomAttributes = call_user_func_array("array_merge", $customAttributes);

        $emailAddressSplit = splitMailadress($emailAddressOnly);
        $emailSplitNamed = splitMailNamed($emailAddressSplit);

        $newLink = $dom->createElement("a", $link->textContent);
        $newLink->nodeValue = $link->textContent;
        $newLink->setAttribute("href", "#");
        $newLink->setAttribute("data-emailpart1", $emailSplitNamed["emailUsername"]);
        $newLink->setAttribute("data-emailpart2", $emailSplitNamed["emailDomain"]);
        $newLink->setAttribute("data-emailpart3", $emailSplitNamed["emailDomainExtension"]);
        foreach ($newCustomAttributes as $key => $value) {
            $newLink->setAttribute($key, $value);
        }
        $link->parentNode->replaceChild($newLink, $link);
    }
}

function getSpamFreeJs(): string
{
    $pathToJs = "media/plugins/vitamin2/v2-spam-free-mail/bundle.js";
    $mod = new Asset($pathToJs);
    $moddate = $mod->modified();
    return $pathToJs . "?v=" . $moddate;
}

function splitMailadress($emailAddress): array
{
    return explode("@", $emailAddress);
}

function splitMailNamed($emailAddressSplit): array
{
    $emailDomainExtension = substr(strrchr($emailAddressSplit[1], "."), 1);
    return [
        "emailUsername" => $emailAddressSplit[0],
        "emailDomainExtension" => $emailDomainExtension,
        "emailDomain" => substr(explode($emailDomainExtension, $emailAddressSplit[1])[0], 0, -1),
    ];
}

function getMailText($emailSplitNamed, $emailText, $emailAddress): string
{
    if ($emailText == $emailAddress) {
        $emailTextValue =
            $emailSplitNamed["emailUsername"] .
            '@<span style="display:none">do-not-want-span</span>' .
            $emailSplitNamed["emailDomain"] .
            "." .
            $emailSplitNamed["emailDomainExtension"];
    } else {
        $emailTextValue = $emailText;
    }
    return $emailTextValue;
}
