<?php
$emailAddressSplit = splitMailadress($emailAddress);
$emailSplitNamed = splitMailNamed($emailAddressSplit);
$otherAttributesStr = "";
foreach ($customAttributes as $key => $value) {
    $otherAttributesStr .= " " . $key . '="' . $value . '"';
}
$newEmailTextValue = getMailText($emailSplitNamed, $emailText, $emailAddress);
?>

<a href="#" data-emailpart1="<?= $emailSplitNamed["emailUsername"] ?>" data-emailpart2="<?= $emailSplitNamed[
    "emailDomain"
] ?>" data-emailpart3="<?= $emailSplitNamed["emailDomainExtension"] ?>" <?= $otherAttributesStr ?>>
<?= $newEmailTextValue ?>
</a>
