export default class EmailController {
  constructor() {
    this.allEmailLinks = document.querySelectorAll("[data-emailpart1][data-emailpart2][data-emailpart3]");
  }
  init() {
    if (this.allEmailLinks) {
      this.allEmailLinks.forEach((el) => {
        el.addEventListener("click", function (e) {
          e.preventDefault();
          const attrs = el.dataset;
          window.open(`mailto:${attrs.emailpart1}@${attrs.emailpart2}.${attrs.emailpart3}`);
        });
      });
    }
  }
}
