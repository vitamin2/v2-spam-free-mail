import EmailController from "./controller/EmailController";

document.addEventListener("DOMContentLoaded", () => {
  const emailController = new EmailController();
  emailController.init();
});
