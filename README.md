# vitamin2/v2-spam-free-mail plugin for Kirby 3

> Just a plugin which decrypts the email addresses on the fly.

****

## Installation

### Download

Download and copy this repository to `/site/plugins/vitamin2/v2-spam-free-mail`.

### Composer

```
composer require vitamin2/v2-spam-free-mail
```

## Usage

1. Install plugin

2. Add Spam Free JS to your template (getSpamFreeJs returns a versionised JS file)

```php
  <?= js([
    getSpamFreeJs(),
  ]) ?>
```

3. Preload it (optional)

```php
  <link rel="preload" href="<?= getSpamFreeJs() ?>" as="script" />
```

4. Use It.
For text fields it will be used automatically. For other fields you can use the snippet.

```php
    <?= snippet("global/protected-mail", [
        "emailAddress" => $site->emailAddress()->value(),
        "emailText" => $site->emailAddress()->value(),
        "customAttributes" => [
            "class" => "link-footer",
            "rel" => "noopener noreferrer",
            "target" => "_blank",
            "title" => "link-footer",
        ],
    ]) ?>
```

## License

MIT
